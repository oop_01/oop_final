/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.manita.finalproject;

import java.io.Serializable;

/**
 *
 * @author user
 */
public class Books implements Serializable{
    private String name;
    private String booktitle;  
    private int data;
    private String status;

    public Books(String name, String booktitle, int data, String status) {
        this.name = name;
        this.booktitle = booktitle;
        this.data = data;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBooktitle() {
        return booktitle;
    }

    public void setBooktitle(String booktitle) {
        this.booktitle = booktitle;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Books{" + "name=" + name + ", booktitle=" + booktitle + ", data=" + data + ", status=" + status + '}';
    }
    
}
